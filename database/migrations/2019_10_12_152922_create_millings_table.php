<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMillingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('millings', function (Blueprint $table) {
            $table->increments('m_id');
            $table->double('m_amount');
            $table->string('m_date');
            $table->integer('id')->references('id')->on('users') ->onDelete('cascade') ->onUpdate('cascade');
            $table->Integer('month');
            $table->Integer('year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('millings');
    }
}
