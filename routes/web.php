<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



 Route::group(['middleware' => 'App\Http\Middleware\AuthMiddleware'], function()
    {
       Route::get('/addmill','HomeController@addmill');
       Route::post('/addmill','millController@create'); 

    });





Route::get('/showmill','millController@showmill')->middleware(AuthMiddleware::class);

Route::get('/searchmill','millController@searchmill');

Route::post('/searchmill','millController@searchmillresult');

Route::get('/addcredit','HomeController@addcredit');

Route::post('/addcredit','creditController@store');

Route::get('/searchcredit','creditController@searchcredit');

Route::post('/searchcredit','creditController@searchcredit1');

Route::get('/adddedit','HomeController@adddebit');

Route::post('/adddedit','debitController@store');

Route::get('/searchdedit','debitController@searchdebit');

Route::post('/searchdedit','debitController@searcheddebit');

Route::get('/millcharge','HomeController@millcharge');

Route::post('/millcharge','millController@millcharge');

Route::get('/groupwisemillcharge','HomeController@groupwisemill');

Route::post('/groupwisemillcharge','millController@groupwisemill');

Route::get('/cashinhand','millController@cashinhand');

Route::post('/cashinhand','millController@cashinhand2');

//Route:: get('/views','millController@viewfile');