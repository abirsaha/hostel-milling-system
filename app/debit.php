<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class debit extends Model
{
    //
     protected $fillable = [
         'd_amount', 'd_date','id','d_month','d_year','description',
    ];
}
