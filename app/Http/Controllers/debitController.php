<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class debitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

         $date = $request->d_date;
        $month_year = explode("-", $date);
        $year = $month_year[0];
        $month=$month_year[1];

        

         DB::table('debits')->insert([
            'd_amount'=>$request->d_amount,
            'd_date'=>$request->d_date,
            'id'=>$request->id,
            'd_month'=>$month,
            'd_year'=>$year,
            'description'=>$request->description,
        ]);

         return redirect('/adddedit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function searchdebit(){
        $months=DB::table('month')
                 ->select('*')
                 ->get();
        $years = DB::table('years')
                ->select('*')
                ->get();

        return view('fontEnd.search.searchdebit',['months'=>$months],['years'=>$years]);
    }

    public function searcheddebit(Request $request){
        $debit=DB::table('debits')
                 ->join('users','users.id','debits.id')
                 ->where('d_month',$request->month)
                 ->where('d_year',$request->year)
                 ->select('*')
                 ->get();
        // echo "<pre>";
        // print_r($months);

         return view('fontEnd.shows.searcheddebit',['debit'=>$debit]); 
    }
}
