<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class creditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $date = $request->c_date;
        $month_year = explode("-", $date);
        $year = $month_year[0];
        $month=$month_year[1];

        

         DB::table('credits')->insert([
            'c_amount'=>$request->c_amount,
            'c_date'=>$request->c_date,
            'id'=>$request->id,
            'c_month'=>$month,
            'c_year'=>$year,
        ]);

         return redirect('/addcredit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function searchcredit(){
        $months=DB::table('month')
                 ->select('*')
                 ->get();
        $years = DB::table('years')
                ->select('*')
                ->get();

        return view('fontEnd.search.searchcredit',['months'=>$months],['years'=>$years]);         

    }

    public function searchcredit1(Request $request){
         $credit=DB::table('credits')
                 ->join('users','users.id','credits.id')
                 ->where('c_month',$request->month)
                 ->where('c_year',$request->year)
                 ->select('*')
                 ->get();
        // echo "<pre>";
        // print_r($months);

         return view('fontEnd.shows.searchedcredit',['credit'=>$credit]); 
    }
}
