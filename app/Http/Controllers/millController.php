<?php

namespace App\Http\Controllers;
use App\milling;
use Illuminate\Http\Request;
use DB;
class millController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
      // milling::create($request->all());
      //   return redirect('/addmill');
        $date = $request->m_date;
        $month_year = explode("-", $date);
        $year = $month_year[0];
        $month=$month_year[1];

        

         DB::table('millings')->insert([
            'm_amount'=>$request->m_amount,
            'm_date'=>$request->m_date,
            'id'=>$request->id,
            'month'=>$month,
            'year'=>$year,
        ]);

         return redirect('/addmill');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showmill(){
        $mill=DB::table('millings')
                 ->join('users','millings.id','users.id')
                 ->select('*')
                 ->get();

         
        return view('fontEnd.shows.showmill',['mill'=>$mill]);
    }

    public function millcharge(Request $request){
        $cost = DB::table('debits')
                ->select(DB::raw("SUM(d_amount)   as cost"))
                ->where('d_year',$request->year)
                ->where('d_month',$request->month)
                ->get();
                

        $totalmill = DB::table('millings')
                    ->select(DB::raw("SUM(m_amount) as totalmill"))
                    ->where('month',$request->month)
                    ->where('year',$request->year)
                    ->get();
                    

           

              return view('fontEnd.shows.millcharge',['cost'=>$cost],['totalmill'=>$totalmill]);      
            
    }

    public function groupwisemill(Request $request){
         $cost = DB::table('debits')
                ->select(DB::raw("SUM(d_amount)   as cost"))
                ->where('d_year',$request->year)
                ->where('d_month',$request->month)
                ->get();

         $totalmill = DB::table('millings')
                    ->select(DB::raw("SUM(m_amount) as totalmill"))
                    ->where('month',$request->month)
                    ->where('year',$request->year)
                    ->get();  


            // $millcharge =  DB::table('debits', 'millings')
            //                 ->select(DB::raw("SUM(d_amount) / SUM(millm_amount)  as cost"))
            //                 ->where('d_year',$request->year)
            //                 ->where('d_month',$request->month)
            //                 ->where('month',$request->month)
            //                 ->where('year',$request->year)
            //                  ->get();

            //                echo "<pre>";
            //                print_r($millcharge);
                    

//          foreach ($cost as $cost ) {
//                             foreach ($totalmill as $totalmill ) {
//                                echo  $millcharge = $cost->cost / $totalmill->totalmill;
//                             }
// }
                          

          // $personmill = DB::table('millings')
          //   ->select('year','month','millings.id','name' ,
          //             DB::raw('sum(m_amount) as totalmills'))
          //   ->join('users','millings.id','users.id')
          //   ->where('month',$request->month)
          //   ->where('year',$request->year)
          //   ->groupBy('millings.id','month','year','name')
          //   ->get(); 

            // echo "<pre>";
            // print_r($personmill);
            // return view('fontEnd.shows.groupwisemillcharge',['millcharge'=>$millcharge],['personmill'=>$personmill]);
           
    }

    public function searchmill(){
        

        return view('fontEnd.search.searchmill');
    }

    public function searchmillresult(Request $request){
        $mills = DB::table('millings')
                 ->select('*')
                 ->join('users','millings.id','users.id')
                 ->where('m_date',$request->date)
                 ->get();
          return view('fontEnd.shows.searchmillresult',['mills'=>$mills]);
             //     echo "<pre>";
             // print_r($mills);
    }

    public function cashinhand(){
         $months=DB::table('month')
                 ->select('*')
                 ->get();
        $years = DB::table('years')
                ->select('*')
                ->get();

        return view('fontEnd.search.searchcredit',['months'=>$months],['years'=>$years]);
    }

    public function cashinhand2(Request $request){
        $credits =  DB::table('credits')
                    ->select(DB::raw("SUM(c_amount) as totalcredit"))
                    ->where('c_month',$request->month)
                    ->where('c_year',$request->year)
                    ->get();

        $debits = DB::table('debits')
                    ->select(DB::raw("SUM(d_amount) as totaldebit"))
                    ->where('d_month',$request->month)
                    ->where('d_year',$request->year)
                    ->get();

        return view('fontEnd.shows.cashinhand',['credits'=>$credits],['debits'=>$debits]);

    }

    public function viewfile(){
       $views = DB::table('allcredits')
                    ->select('*') 
                    ->get();

        echo "<pre>";
        print_r($views);

    }

} 
