<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\User;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function addmill(){
        $users=User::all();
        return view('fontEnd.entrymill',['users'=>$users]);
    }

     public function addcredit(){
        $users=User::all();
        return view('fontEnd.addcredit',['users'=>$users]);
    }

    public function adddebit(){
        $users=User::all();
        return view('fontEnd.adddebit',['users'=>$users]);
    }


    public function millcharge(){
        $months=DB::table('month')
                 ->select('*')
                 ->get();
        $years = DB::table('years')
                ->select('*')
                ->get();

        return view('fontEnd.search.searchdebit',['months'=>$months],['years'=>$years]);
    }

    public function groupwisemill(){
         $months=DB::table('month')
                 ->select('*')
                 ->get();
        $years = DB::table('years')
                ->select('*')
                ->get();

        return view('fontEnd.search.searchdebit',['months'=>$months],['years'=>$years]);
    }
}
